#!/bin/bash

set -e

if [ -z "$1" ]; then
  wait-for-it gitlab.com:22 --timeout=30 --strict -- java -jar app.jar
fi

exec "$@"
