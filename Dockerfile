FROM micro8734/deployer as build

FROM openjdk:8-jre-slim

RUN apt-get update && apt-get install -y wait-for-it netcat

RUN groupadd application && useradd application -g application -d /application -m

COPY --from=build /application/target/configuration-server-0.0.1-SNAPSHOT.jar /application/app.jar

COPY docker-entrypoint.sh /usr/local/bin/

USER application

WORKDIR /application

HEALTHCHECK --interval=1m --timeout=3s --start-period=30s --retries=5 CMD nc -vz localhost ${SERVER_PORT:-8888}

ENTRYPOINT ["docker-entrypoint.sh"]
